/// a cli for the time-tracker tool
#[derive(Debug, clap::Parser)] // requires `derive` feature
#[command(name = "ttt")]
#[command(about = "a time-tracking tool written in rust", long_about = None)]
pub struct Cli {
    #[command(subcommand)]
    pub command: Commands,
}

#[derive(Debug, clap::Subcommand)]
pub enum Commands {
    /// adds activities to database
    #[command(arg_required_else_help = true)]
    Add {
        /// name of project to add
        #[arg(required = true, short, long)]
        project: String,
        /// start time: YYYY-MM-DD hh:mm:ss
        #[arg(required = true, long)]
        start: String,
        /// stop time: YYYY-MM-DD hh:mm:ss
        #[arg(required = true, long)]
        stop: String,
        /// add some tags for the activity: tag0, tag1
        tags: Vec<String>,
    },

    /// change existing activity
    #[command(arg_required_else_help = true)]
    Change {
        /// uuid of activity
        #[arg(required = true, short, long)]
        id: uuid::Uuid,
        /// change the project name
        #[arg(short, long, default_value = "")]
        project: String,
        /// start time: YYYY-MM-DD hh:mm:ss
        #[arg(long, default_value = "")]
        start: String,
        /// stop time: YYYY-MM-DD hh:mm:ss
        #[arg(long, default_value = "")]
        stop: String,
        /// add some tags for the activity: tag0, tag1
        /// all previous tags will be overwritten
        #[arg(required = false)]
        tags: Vec<String>,
    },

    /// export the database
    Export {},

    /// get activity by id
    Get {
        /// uuid of activity
        #[arg(required = true, short, long)]
        id: uuid::Uuid,
    },

    /// list all activities
    List {
        /// define reporting period
        #[arg(short, long, value_parser(["y", "m", "w", "3"]))]
        period: String,
        /// output in json format, optional
        #[arg(short, long)]
        json: bool,
    },

    /// get activity by id
    Remove {
        /// uuid of activity
        #[arg(required = true, short, long)]
        id: uuid::Uuid,
    },

    /// report on all activities
    /// aggragate project durations
    Report {
        #[arg(short, long, value_parser(["y", "m", "w", "3"]))]
        period: String,
    },

    /// start working on a project
    Start {
        /// name of project
        #[arg(required = true, short, long)]
        project: String,
        /// add some tags for the activity: tag,0, tag1
        tags: Vec<String>,
    },

    /// stop working on current project
    Stop {
        /// name of project, if project is omitted, it will stop the last unfinished
        /// project
        #[arg(required = true, short, long)]
        id: uuid::Uuid,
    },
}
