use std::fs;

use serde::Deserialize;

#[derive(Debug, Deserialize)]
struct Config {
    db_path: String,
    db_name: String,
}

#[derive(Debug)]
pub struct Database {
    pub db: sled::Db,
}

impl Default for Database {
    fn default() -> Self {
        let configfile: String = fs::read_to_string("./ttt.toml").unwrap_or("".to_string());
        let config: Config = toml::from_str(&configfile).unwrap_or(Config {
            db_path: "/home/dorian/.local/".to_string(),
            db_name: "activities.db".to_string(),
        });

        Self {
            db: match sled::open(config.db_path + &config.db_name) {
                Ok(db) => db,
                Err(e) => panic!("Cannot open database {e:?}"),
            },
        }
    }
}

pub trait Crud {
    fn create(&self, id: uuid::Uuid, act: String);
    fn read(&self, id: uuid::Uuid) -> String;
    fn update(&self, id: uuid::Uuid, act_new: String);
    fn delete(&self, id: uuid::Uuid) -> String;
}

pub trait Export {
    fn exp(&self);
    fn get_all(&self) -> sled::Iter;
}

impl Crud for Database {
    fn create(&self, id: uuid::Uuid, act: String) {
        let _ = &self.db.insert(id.to_string(), act.as_str());
    }

    fn read(&self, id: uuid::Uuid) -> String {
        let value = &self.db.get(id.to_string()).unwrap().unwrap();
        let ret: String = std::str::from_utf8(value.as_ref()).unwrap().to_string();
        ret
    }

    fn update(&self, id: uuid::Uuid, act_new: String) {
        let old_act: String = self.read(id);
        let _ = &self.db.compare_and_swap(
            id.to_string(),
            Some(old_act.as_str()),
            Some(act_new.as_str()),
        );
    }

    fn delete(&self, id: uuid::Uuid) -> String {
        let value = &self.db.remove(id.to_string()).unwrap().unwrap();
        let ret: String = std::str::from_utf8(value.as_ref()).unwrap().to_string();
        ret
    }
}

impl Export for Database {
    fn exp(&self) {}

    fn get_all(&self) -> sled::Iter {
        let key: (sled::IVec, sled::IVec) = match self.db.first() {
            Ok(Some(value)) => value,
            Ok(None) => panic!(""),
            Err(e) => panic!("{e}"),
        };
        let scan_key: &[u8] = &key.0;

        self.db.range(scan_key..)
    }
}
