/// this is a time tracking app
///
mod activity;
mod args;
mod db;
mod project;

use std::collections::HashMap;

use crate::{
    activity::{Activity, Deser, Display},
    db::Export,
};
use args::{Cli, Commands};
use chrono::{Datelike, Duration, NaiveDateTime, TimeZone, Utc};
use clap::Parser;
use colored::Colorize;
use db::{Crud, Database};

const TD_FORMAT: &str = "%Y-%m-%d %H:%M:%S";

fn pretty_print(input: String, action: &str) -> String {
    let out: String = match action {
        "command" => {
            format!("{}", input.truecolor(0, 255, 200))
        }
        "border" => {
            format!("{}", input.truecolor(0, 150, 200))
        }
        _ => "".to_string(),
    };

    out
}

/// this is the main function
fn main() {
    // parsing command line arguments
    let args = Cli::parse();
    // initialzing the database (sled)
    let db: Database = Database {
        ..Default::default()
    };

    match args.command {
        Commands::Add {
            project,
            start,
            stop,
            tags,
        } => {
            println!("{}", pretty_print("adding activity".to_string(), "command"));

            let start_dt = NaiveDateTime::parse_from_str(&(start), TD_FORMAT).unwrap();
            let stop_dt = NaiveDateTime::parse_from_str(&(stop), TD_FORMAT).unwrap();

            let act: Activity = Activity {
                project,
                start: start_dt.timestamp(),
                stop: stop_dt.timestamp(),
                tags,
                ..Default::default()
            };

            act.display();

            db.create(act.id, act.serialize());
        }

        Commands::Change {
            id,
            project,
            start,
            stop,
            tags,
        } => {
            println!(
                "{} {id:?}",
                pretty_print("changing existing activity".to_string(), "command")
            );

            let mut act: Activity = Activity::deserialize(db.read(id));
            if !project.is_empty() {
                act.project = project;
            }
            if !start.is_empty() {
                act.start = NaiveDateTime::parse_from_str(&(start), TD_FORMAT)
                    .unwrap()
                    .timestamp();
            }
            if !stop.is_empty() {
                act.stop = NaiveDateTime::parse_from_str(&(stop), TD_FORMAT)
                    .unwrap()
                    .timestamp();
            }
            if !tags.is_empty() {
                act.tags = tags;
            }
            db.delete(id);
            db.create(act.id, act.serialize());
            act.display();
        }

        Commands::Export {} => {
            println!(
                "{}",
                pretty_print("exporting database".to_string(), "command")
            );

            db.exp();
        }

        Commands::Get { id } => {
            println!(
                "{}",
                pretty_print("get activity by id".to_string(), "command")
            );

            let act: Activity = Activity::deserialize(db.read(id));
            act.display();
        }

        Commands::List { period, json } => {
            print!(
                "{}",
                pretty_print("listing all activities ".to_string(), "command")
            );

            match period.as_str() {
                "y" => println!("{}", "for current year".truecolor(0, 200, 200)),
                "m" => println!("{}", "for current month".truecolor(0, 200, 200)),
                "w" => println!("{}", "for current week".truecolor(0, 200, 200)),
                "3" => println!("{}", "for last 365 days".truecolor(0, 200, 200)),
                _ => println!("{}", "None".truecolor(200, 0, 0)),
            }

            let iter: sled::Iter = db.get_all();
            iter.for_each(|resp| match resp {
                Ok(res) => {
                    let act_str = std::str::from_utf8(&res.1).unwrap().to_string();
                    let act = Activity::deserialize(act_str);
                    let st = Utc.timestamp_opt(act.start, 0).unwrap();

                    match period.as_str() {
                        "y" => {
                            if st.year() == Utc::now().year() {
                                if !json {
                                    act.display();
                                } else {
                                    act.display_json();
                                }
                            }
                        }
                        "m" => {
                            if st.month() == Utc::now().month() {
                                if !json {
                                    act.display();
                                } else {
                                    act.display_json();
                                }
                            }
                        }
                        "w" => {
                            if st.iso_week() == Utc::now().iso_week() {
                                if !json {
                                    act.display();
                                } else {
                                    act.display_json();
                                }
                            }
                        }
                        "3" => {
                            if st >= Utc::now() - Duration::days(365) {
                                if !json {
                                    act.display();
                                } else {
                                    act.display_json();
                                }
                            }
                        }
                        _ => println!("{}", "None".truecolor(200, 0, 0)),
                    }
                }
                Err(_) => todo!(),
            });
        }

        Commands::Remove { id } => {
            println!(
                "{}",
                pretty_print("remove activity by id".to_string(), "command")
            );
            let act: Activity = Activity::deserialize(db.delete(id));
            act.display();
        }

        Commands::Report { period } => {
            print!(
                "{}",
                pretty_print("report on projects ".to_string(), "command")
            );
            match period.as_str() {
                "y" => println!("{}", "for current year".truecolor(0, 200, 200)),
                "m" => println!("{}", "for current month".truecolor(0, 200, 200)),
                "w" => println!("{}", "for current week".truecolor(0, 200, 200)),
                "3" => println!("{}", "for last 365 days".truecolor(0, 200, 200)),
                _ => println!("{}", "None".truecolor(200, 0, 0)),
            }

            let mut projects: HashMap<String, i64> = HashMap::new();
            let mut dur: i64 = 0;

            let iter: sled::Iter = db.get_all();
            iter.for_each(|resp| match resp {
                Ok(res) => {
                    let act_str = std::str::from_utf8(&res.1).unwrap().to_string();
                    let act = Activity::deserialize(act_str);
                    let st = Utc.timestamp_opt(act.start, 0).unwrap();

                    match period.as_str() {
                        "y" => {
                            if st.year() == Utc::now().year() {
                                dur = match projects.get(&act.project) {
                                    Some(dur) => *dur,
                                    None => 0,
                                };
                                dur += act.stop - act.start;
                                projects.insert(act.project, dur);
                            }
                        }
                        "m" => {
                            if st.month() == Utc::now().month() {
                                dur = match projects.get(&act.project) {
                                    Some(dur) => *dur,
                                    None => 0,
                                };
                                dur += act.stop - act.start;
                                projects.insert(act.project, dur);
                            }
                        }
                        "w" => {
                            if st.iso_week() == Utc::now().iso_week() {
                                dur = match projects.get(&act.project) {
                                    Some(dur) => *dur,
                                    None => 0,
                                };
                                dur += act.stop - act.start;
                                projects.insert(act.project, dur);
                            }
                        }
                        "3" => {
                            if st >= Utc::now() - Duration::days(365) {
                                dur = match projects.get(&act.project) {
                                    Some(dur) => *dur,
                                    None => 0,
                                };
                                dur += act.stop - act.start;
                                projects.insert(act.project, dur);
                            }
                        }
                        _ => println!("{}", "None".truecolor(200, 0, 0)),
                    }
                }
                Err(_) => todo!(),
            });

            println!(
                "{}{:\u{2550}<25}{}",
                "\u{2554}".truecolor(0, 150, 200),
                "".truecolor(0, 150, 200),
                "\u{2557}".truecolor(0, 150, 200)
            );
            println!(
                "{} {:^10} | {:^10} {}",
                "\u{2551}".truecolor(0, 150, 250),
                "project",
                "duration",
                "\u{2551}".truecolor(0, 150, 250)
            );
            println!(
                "{}{:\u{2550}<25}{}",
                "\u{2560}".truecolor(0, 150, 250),
                "".truecolor(0, 150, 250),
                "\u{2563}".truecolor(0, 150, 250)
            );

            for (project, duration) in projects {
                let hours = Duration::seconds(duration).num_hours();
                let mins = Duration::seconds(duration - (hours * 60 * 60)).num_minutes();
                println!(
                    "{} {project:<10} | {hours:>5}h{mins:>3}m {}",
                    "\u{2551}".truecolor(0, 150, 250),
                    "\u{2551}".truecolor(0, 150, 250)
                );
            }

            println!(
                "{}{:\u{2550}<25}{}",
                "\u{255a}".truecolor(0, 150, 250),
                "".truecolor(0, 150, 250),
                "\u{255d}".truecolor(0, 150, 250)
            );
        }

        Commands::Start { project, tags } => {
            println!(
                "{}",
                pretty_print("starting activity".to_string(), "command")
            );
            let act: Activity = Activity {
                project,
                tags,
                ..Default::default()
            };
            db.create(act.id, act.serialize());
            act.display_start();
        }

        Commands::Stop { id } => {
            println!(
                "{}",
                pretty_print("stopping activity".to_string(), "command")
            );
            let mut act: Activity = Activity::deserialize(db.read(id));
            act.stop = Utc::now().timestamp();
            db.delete(id);
            db.create(act.id, act.serialize());
            act.display();
        }
    }
}
