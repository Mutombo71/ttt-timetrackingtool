use chrono::{NaiveDateTime, Utc};
use colored::Colorize;
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct Activity {
    pub id: uuid::Uuid,
    pub project: String,
    pub start: i64,
    pub stop: i64,
    pub tags: Vec<String>,
}

pub trait Display {
    fn display(&self);
    fn display_start(&self);
    fn display_json(&self);
}

pub trait Deser {
    fn serialize(&self) -> String;
    fn deserialize(s: String) -> Activity;
}

pub trait Commands {
    fn add(&self);
    fn list(&self);
    fn start(&self);
    fn stop(&self);
}

impl Default for Activity {
    fn default() -> Self {
        Self {
            id: uuid::Uuid::new_v4(),
            project: "".to_string(),
            start: Utc::now().naive_local().timestamp(),
            stop: Utc::now().naive_local().timestamp(),
            tags: vec![],
        }
    }
}

impl Display for Activity {
    fn display(&self) {
        println!(
            "project: {0:} ({1:})",
            self.project.truecolor(0, 255, 250),
            self.id
        );
        for tag in &self.tags {
            print!("\t{:0}", tag.truecolor(0, 192, 0));
        }

        let st: NaiveDateTime = NaiveDateTime::from_timestamp_opt(self.start, 0).unwrap();
        let end: NaiveDateTime = NaiveDateTime::from_timestamp_opt(self.stop, 0).unwrap();
        let diff: chrono::Duration = chrono::Duration::seconds(self.stop - self.start);

        println!(
            "\t{0:} <-> {1:} duration: {2:}m",
            st,
            end,
            diff.num_minutes()
        );
    }

    fn display_start(&self) {
        println!("project: {0:} ({1:})", self.project, self.id);
        for tag in &self.tags {
            print!("\t{tag}");
        }

        let st: NaiveDateTime = NaiveDateTime::from_timestamp_opt(self.start, 0).unwrap();

        println!("\t{0:} <-> undef", st);
    }

    fn display_json(&self) {
        // TODO: check why not only json string is displayed
        println!("{}", serde_json::to_string(&self).unwrap());
    }
}

impl Deser for Activity {
    fn serialize(&self) -> String {
        serde_json::to_string(&self).unwrap()
    }
    fn deserialize(s: String) -> Activity {
        serde_json::from_str(&s).unwrap()
    }
}

impl Commands for Activity {
    fn add(&self) {
        // TODO: implement add function
        todo!("implement add function")
    }
    fn list(&self) {
        // TODO: implement list function
        todo!("implement list function")
    }
    fn start(&self) {
        // TODO: implement start function
        todo!("implement start function")
    }
    fn stop(&self) {
        // TODO: implement stop function
        todo!("implement stop function")
    }
}
