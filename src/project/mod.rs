#[derive(Debug)]
pub struct Project {
    pub id: uuid::Uuid,
    pub project: String,
}
impl Default for Project {
    fn default() -> Self {
        Self {
            id: uuid::Uuid::new_v4(),
            project: "".to_string(),
        }
    }
}

impl Project {
    fn _duration(&mut self, start: i64, stop: i64) -> i64 {
        stop - start
    }
}
